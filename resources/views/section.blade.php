@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row" style="padding-bottom: 20px">
            <div class="col-md-1">
                Номер
            </div>
            <div class="col-md-6">
                Критерий
            </div>
            <div class="col-md-1">
                Выполнен
            </div>
            <div class="col-md-3">
                Комментарий
            </div>
            <div class="col-md-1">
                Баллы
            </div>
        </div>
        <form class="form-horizontal">
            @for ($i = 0; $i < 10; $i++)
                <div class="form-group">
                    <div class="col-md-1">
                        {{$i + 1}}.
                    </div>
                    <div class="col-md-6">
                        <input class="form-control">
                    </div>
                    <div class="col-md-1">
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Да
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="#">Да</a></li>
                                <li><a href="#">Нет</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <input class="form-control">
                        </input>
                    </div>
                    <div class="col-md-1">
                        <input class="form-control">
                    </div>
                </div>
            @endfor
        </form>
    </div>
@endsection
