@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row" style="padding-bottom: 20px">
            <div class="col-md-1">
                Номер
            </div>
            <div class="col-md-9">
                Критерий
            </div>
            <div class="col-md-2">
                Баллы
            </div>
        </div>
        <form class="form-horizontal">
            @for ($i = 0; $i < 4; $i++)
                <div class="form-group">
                    <div class="col-md-1">
                        {{$i + 1}}.
                    </div>
                    <div class="col-md-9">
                        <input class="form-control">
                    </div>
                    <div class="col-md-2">
                        25
                    </div>
                </div>
            @endfor
        </form>
    </div>
@endsection
